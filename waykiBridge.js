var waykiBridgeObj = (function (winodw) {
  // 设备环境集合
  var ua = navigator.userAgent,
    bridgeMap =
    {
      Android: connectWebViewJavascriptBridge,
      Ios: setupWebViewJavascriptBridge
    },
    client = {
      isAndroid: /Android/i.test(ua),
      isIos: /iPhone|iPod|iPad/i.test(ua)
    }
  // 获取当前设备环境对应的bridge方法
  var currentBridge = (client.isAndroid && bridgeMap.Android) || (client.isIos && bridgeMap.Ios) || null;

  // 唤起app方法
  function callhandler(name, query, callback) {
    currentBridge(function (bridge) {
      bridge.callHandler(name, query, callback);
    });
  }

  // 注册app唤起js方法
  function registerhandler(name, callback) {
    currentBridge(function (bridge) {
      bridge.registerHandler(name, function (data, responseCallback) {
        callback(data, responseCallback);
      });
    });
  }

  function webPluginError(err, errorCallBack) {
    var res = {};
    if (err.message == "Please create wallet first") {
      res.errorMsg = "Please create or import wallet first.";
      res.errorCode = 2000;
    } else if (err.message == "User denied transaction signature.") {
      res.errorMsg = err.message;
      res.errorCode = 7000;
    } else {
      res.errorMsg = err.message;
      res.errorCode = 9000;
    }
    errorCallBack(res);
  }


  // web plugin method invoke
  function callWebPlugin(name, query, callback, error) {
    switch (name) {
      // get account info
      case "getAddressInfo":
        var res = {};
        WiccWallet.getDefaultAccount().then(
          function (resp) {
            res.result = resp;
            res.errorCode = 0;
            callback(res);
          },
          function (err) {
            webPluginError(err, error);
          }
        );
        break;
      // contract invoke
      case "walletPluginContractInvoke":
        var res = {};
        WiccWallet.callContract(
          query.regId,
          query.contractField,
          query.inputAmount,
          function (err, data) {
            if (err === null) {
              res.result = data;
              res.errorCode = 0;
              return callback(res);
            }
            webPluginError(err, error);
          }
        ).then(
          function () { },
          function (err) {
            webPluginError(err, error);
          }
        );
        break;
      // pubish smart contract
      case "walletPluginContractIssue":
        var res = {};
        WiccWallet.publishContract(
          query.contractContent,
          query.contractDesc,
          function (err, data) {
            if (err === null) {
              res.result = data;
              res.errorCode = 0;
              return callback(res);
            }
            webPluginError(err, error);
          }
        ).then(
          function () { },
          function (err) {
            webPluginError(err, error);
          }
        );
        break;
      // transfer wicc
      case "walletPluginTransfer":
        var res = {};
        WiccWallet.requestPay(
          query.collectionAddress,
          Number(query.amount),
          "",
          function (err, data) {
            if (err === null) {
              res.result = data;
              res.errorCode = 0;
              return callback(res);
            }
            webPluginError(err, error);
          }
        ).then(
          function () { },
          function (err) {
            webPluginError(err, error);
          }
        );
        break;
      // 节点投票
      case "walletPluginNodeVote":
        // 数据重组
        var nodeList = query.nodeList.map(function (item) {
          return {
            address: item.votedAddress,
            votes: Number(item.voteCount)
          };
        }),
          res = {};
        WiccWallet.requestVote(nodeList, function (err, data) {
          if (err === null) {
            res.result = data;
            res.errorCode = 0;
            return callback(res);
          }
          webPluginError(err, error);
        }).then(
          function () { },
          function (err) {
            webPluginError(err, error);
          }
        );
        break;
      case "walletPluginUCoinTransfer":
        var res = {};
        WiccWallet.UCoinTransfer(query.destArr, query.memo, function (
          err,
          data
        ) {
          if (err === null) {
            res.result = data;
            res.errorCode = 0;
            return callback(res);
          }
          webPluginError(err, error);
        }, query.genSign).then(
          function () { },
          function (err) {
            webPluginError(err, error);
          }
        );
        break;
      case "walletPluginUContractInvoke":
        var res = {};
        WiccWallet.UCoinContractInvoke(
          query.amount,
          query.coinSymbol,
          query.regId,
          query.contract,
          query.memo,
          function (err, data) {
            if (err === null) {
              res.result = data;
              res.errorCode = 0;
              return callback(res);
            }
            webPluginError(err, error);
          },
          query.genSign
        ).then(
          function () { },
          function (err) {
            webPluginError(err, error);
          }
        );
        break;
      case "walletPluginAssetIssue":
        var res = {};
        WiccWallet.AssetIssue(
          query.assetSymbol,
          query.assetName,
          query.assetSupply,
          query.assetOwnerId,
          query.assetMintable,
          function (err, data) {
            if (err === null) {
              res.result = data;
              res.errorCode = 0;
              return callback(res);
            }
            webPluginError(err, error);
          }
        ).then(
          function () { },
          function (err) {
            webPluginError(err, error);
          }
        );
        break;
      case "walletPluginAssetUpdate":
        var res = {};
        WiccWallet.AssetUpdate(
          query.assetSymbol,
          query.updateType,
          query.updateValue,
          function (err, data) {
            if (err === null) {
              res.result = data;
              res.errorCode = 0;
              return callback(res);
            }
            webPluginError(err, error);
          }
        ).then(
          function () { },
          function (err) {
            webPluginError(err, error);
          }
        );
        break;
      case "signMessage":
        var res = {};
        WiccWallet.SignMessage(
          query.dappLogo,
          query.dappName,
          query.message,
          function (err, data) {
            if (err === null) {
              res.result = data;
              res.errorCode = 0;
              return callback(res);
            }
            webPluginError(err, error);
          }
        ).then(
          function () { },
          function (err) {
            webPluginError(err, error);
          }
        );
        break;
    }
  }

  //webview交互对象
  function webviewActive() { }

  webviewActive.prototype.walletPlugin = function (
    name,
    query,
    callback,
    error
  ) {
    if (currentBridge) {
      // app接口调用
      return callhandler(name, query, function (res) {
        var response =
          Object.prototype.toString.call(res) === "[object Object]"
            ? res
            : JSON.parse(res);
        if (response.errorCode == 0) {
          callback(response);
        } else {
          error(response);
        }
      });
    } else {
      // web wallet plugin api invoke
      try {
        callWebPlugin(name, query, callback, error);
      } catch (err) {
        if (typeof err === "object") {
          if (
            err.message == "WiccWallet is not defined" ||
            err.message == "Can't find variable: WiccWallet"
          ) {
            error({
              errorMsg: "Please install WICC wallet first.",
              errorCode: 1000
            });
          } else {
            error({
              errorMsg: err.message,
              errorCode: 9000
            });
          }
        } else {
          error({
            errorMsg: err,
            errorCode: 9000
          });
        }
      }
    }
  };

  // XMLHttpRequest封装
  function http(option) {
    option.data = option.data || {};
    var xhr = new XMLHttpRequest();
    xhr.responseType = option.responseType || "json";
    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4) {
        if (xhr.status === 200) {
          if (option.success && typeof option.success === "function") {
            // 请求成功回调
            option.success(xhr.response);
          }
        } else {
          if (option.error && typeof option.error === "function") {
            // 请求失败回调
            option.error();
          }
        }
      }
    };
    xhr.open(option.method, option.url, true);
    if (option.method === "POST") {
      xhr.setRequestHeader("Content-Type", "application/json");
    }
    xhr.send(option.method === "POST" ? JSON.stringify(option.data) : null);
  }

  // ios bridge
  function setupWebViewJavascriptBridge(callback) {
    if (window.WebViewJavascriptBridge) {
      return callback(window.WebViewJavascriptBridge);
    }
    if (window.WVJBCallbacks) {
      return window.WVJBCallbacks.push(callback);
    }
    window.WVJBCallbacks = [callback];
    var WVJBIframe = document.createElement("iframe");
    WVJBIframe.style.display = "none";
    WVJBIframe.src = "wvjbscheme://__BRIDGE_LOADED__";
    document.documentElement.appendChild(WVJBIframe);
    setTimeout(function () {
      document.documentElement.removeChild(WVJBIframe);
    }, 0);
  }

  // Android bridge
  function connectWebViewJavascriptBridge(callback) {
    if (window.WebViewJavascriptBridge) {
      callback(WebViewJavascriptBridge);
    } else {
      document.addEventListener(
        "WebViewJavascriptBridgeReady",
        function () {
          callback(WebViewJavascriptBridge);
        },
        false
      );
    }
  }
  return new webviewActive();
})(window);

if (typeof window.waykiBridge == "undefined") {
  window.waykiBridge = waykiBridgeObj;
}
